const { MongoClient } = require("mongodb")
const { connectionString, databaseName } = require("./db-connection")

const client = new MongoClient(connectionString)

async function run() {
  try {
    await client.connect()
    await client.db("admin").command({ ping: 1 })
    console.log("Successfully connected!")
    const database = client.db(databaseName)
    const collection = database.collection("people")
    await collection.insertOne({
    	firstName: "Pietro",
    	lastName: "Martinelli",
    	nickName: "pietrom",
    	birthDay: new Date("1978-03-19"),
    	job: {
    		company: "CodicePlastico Srl",
    		fromYear: 2015
    	}
    })
    console.log("One person inserted!")
  } finally {
    await client.close()
  }
}
run().catch(console.dir)