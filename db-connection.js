const connectionString = "mongodb://172.31.100.100:27017"
const databaseName = "sample"

module.exports = {
	connectionString,
	databaseName
}