const { MongoClient } = require("mongodb")
const { connectionString, databaseName } = require("./db-connection")

const client = new MongoClient(connectionString)

async function run() {
  try {
    await client.connect()
    console.log("Successfully connected!")
    const database = client.db("registry")
    const collection = database.collection("people")
    const people = await collection
      .find({lastName: "Martinelli"})
      .sort({firstName: 1})
      .toArray()

    for(let person of people) {
      console.log('--', person.firstName)
    }

    const people2 = await collection
      .find({lastName: "Martinelli"}, {sort: {firstName: -1}})

    for await(let person of people2) {
      console.log('==', person.firstName)
    }

    const people3 = await collection
      .find({lastName: "Martinelli"})
      .sort({firstName: -1})

    for await(let person of people3) {
      console.log('++', person.firstName)
    }
  } finally {
    await client.close()
  }
}
run().catch(console.dir)