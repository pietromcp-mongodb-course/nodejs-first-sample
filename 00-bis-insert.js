const { MongoClient } = require("mongodb")
const { connectionString, databaseName } = require("./db-connection")

const client = new MongoClient(connectionString)

async function run() {
  try {
    await client.connect()
    const database = client.db(databaseName)
    const collection = database.collection("node-people")
    await collection.insertOne({
    	_id: 19,
    	firstName: "Pietro",
    	lastName: "Martinelli"
    })
  } finally {
    await client.close()
  }
}
run().catch(console.dir)